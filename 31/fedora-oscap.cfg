##############################################################################
## fedora-oscap
## Copyright (C) 2019 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/CONFIG.conf
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## Fedora User Documentation
## https://docs.fedoraproject.org/en-US/docs/
##
## Fedora 19 Security Guide
## https://docs.fedoraproject.org/en-US/Fedora/19/html/Security_Guide/index.html
##
## OpenSCAP Security Guide to the Secure Configuration of 
## Fedora
## https://static.open-scap.org/ssg-guides/ssg-fedora-guide-standard.html
##
## CCE = Common Configuration Enumeration
## https://nvd.nist.gov/cce/index.cfm
##
## Red Hat Enterprise Linux 8 Security Hardening
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/index
##
## CIS Red Hat Enterprise Linux 8 Benchmark v1.0.0
## https://www.cisecurity.org/cis-benchmarks/
##
## These scripts are inspired by various Benchmarks by The Center for 
## Internet Security. However, these scripts have not been reviewed or 
## approved by CIS and they do not guarantee that their use will result 
## in compliance with the CIS baseline. 
##
## CentOS is not an exact copy of Red Hat Enterprise Linux. There may be 
## configuration differences that produce false positives and/or false 
## negatives. If this occurs please file a bug report. CentOS has its own 
## build system, compiler options, patchsets, and is a community supported,
## non-commercial operating system. CentOS does not inherit certifications 
## or evaluations from Red Hat Enterprise Linux. As such, some configuration
## rules (such as those requiring FIPS 140-2 encryption) will continue to fail
## on CentOS. Members of the CentOS community are invited to participate in 
## OpenSCAP and SCAP Security Guide development.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this 
## guidance assume no responsibility whatsoever for its use by other parties,
## and makes no guarantees, expressed or implied, about its quality, 
## reliability, or any other characteristic.
##
##############################################################################
## Notes
## Modify this template with: sed -i.bak 's#fedora-oscap#fedora-newfile#g' fedora-newfile.cfg
##
##############################################################################

%packages

#@security-tools
openscap
openscap-scanner
openscap-utils
scap-security-guide
scap-security-guide-doc
scap-workbench

xmlsec1 
xmlsec1-openssl

unzip
tar
gzip

%end

%post --log=/root/fedora-oscap.log

#timestamp
echo "** fedora-oscap START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/Backup/oscap
OSCAPDIR=/root/openscap_data

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi
if [ ! -d "${OSCAPDIR}" ];  then mkdir -p ${OSCAPDIR};  fi

####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.



cat > ${BACKUPDIR}/audit_security.sh << 'EOF'
# Security Tests

printf "========================================\n"
printf "= Performing Security Tests = $(date +%F) at $(date +%H%M) =\n"
printf "========================================\n"
printf "List all filesystems: \n\n"
df --local -P | awk {'if (NR!=1) print $6'}

printf "========================================\n"
printf "Show system executables that don't have root ownership: \n\n"
find /bin/ /usr/bin/ /usr/local/bin/ /sbin/ /usr/sbin/ /usr/local/sbin/ /usr/libexec \! -user root -exec ls -l {} \;

printf "========================================\n"
printf "Show files that differ from expected file hashes\n"
printf "These will report files modified due to hardening: \n\n"
rpm -Va | grep '^..5'

printf "========================================\n"
printf "Find SUID Executables in local filesystems:\n\n"
# CIS 6.1.13 Audit SUID executables
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -4000 -print
# to search selected file systems use:
# find ${filesystem} -xdev -type f -perm -4000

printf "========================================\n"
printf "Verfiy integrity of the SUID binaries returned by above:\n\n"
SUIDFILES=$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f -perm -4000 -print)
for I in $SUIDFILES; do echo "Integrity of $I:  "; rpm -V $(rpm -qf $I ); echo; done

printf "========================================\n"
printf "Find SGID Executables in local filesystems:\n\n"
# CIS 6.1.14 Audit SGID executables
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -2000 -print
# to search selected file systems use:
# find ${filesystem} -xdev -type f -perm -2000

printf "========================================\n"
printf "Verfiy integrity of SGID binaries returned by above:\n\n"
SGIDFILES=$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f -perm -2000 -print)
for I in $SGIDFILES; do echo "Integrity of $I:  "; rpm -V $(rpm -qf $I ); echo; done

printf "========================================\n"
printf "Show all World-Writable Directories that don't have the Sticky Bits Set:\n\n"
# CIS 1.1.21 Ensure sticky bit is set on all world-writable directories
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null

printf "========================================\n"
printf "Show all World-Writable Files:\n\n"
# CIS 6.1.10 Ensure no world writable files exist
# Ensure No World-Writable Files Exist
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -0002

printf "========================================\n"
printf "Show Un-owned Files and Directories in local file systems\n\n"
# Ensure All Files Are Owned by a User
# CIS 6.1.11 Ensure no unowned files or directories exist 
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -nouser -ls
# to search selected file systems use:
# find ${filesystem} -xdev -nouser

printf "========================================\n"
printf "Show Un-grouped Files and Directories in local file systems\n\n"
# Ensure All Files Are Owned by a Group
# CIS 6.1.12 Ensure no ungrouped files or directories exist
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -nogroup -ls
# to search selected file systems use:
# find ${filesystem} -xdev -nogroup

printf "========================================\n"
printf "Ensure All World-Writable Directories Are Owned by a System Account\n\n"
# Ensure All World-Writable Directories Are Owned by a System Account
# Assumes system accounts have uid < $FIRSTUSER
FIRSTUSER=1000
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type d -perm -0002 -uid +$FIRSTUSER -print

printf "========================================\n"
printf "Verify integrity of passwd, shadow, and group files\n\n"
# Verify integrity of passwd shadow and group files

pwck -r
grpck -r 

printf "========================================\n"
printf "Show all empty password fields\n\n"
# CIS 6.2.1 Ensure password fields are not empty
/bin/cat /etc/shadow | /bin/awk -F: '($2 == "" ) { print $1 " does not have a password "}' 

printf "========================================\n"
printf "Show umask in bashrc and profile\n\n"
# CIS 5.4.4 Ensure default user umask is 027 or more restrictive
grep "umask" /etc/bashrc /etc/profile /etc/profile.d/*.sh

printf "========================================\n"
printf "Show user directories with .rhosts, .netrc, or .forward files\n\n"
# CIS 6.2.14 Ensure no users have .rhosts files
# CIS 6.2.12 Ensure no users have .netrc files
# CIS 6.2.11 Ensure no users have .forward files
HOMEDIR=$(/bin/cat /etc/passwd | /bin/egrep -v '(root|halt|sync|shutdown)' \
 | /bin/awk -F: '($7 != "/sbin/nologin") { print $6 }')
for file in ${HOMEDIR}/.rhosts; do
    if [ ! -h "${FILE}" -a -f "${FILE}" ]; then
      echo ".rhosts file ${HOMEDIR}/.rhosts exists"
      echo ".netrc file ${HOMEDIR}/.netrc exists"
      echo ".forward file ${HOMEDIR}/.forward exists"
    fi 
done

printf "========================================\n"
printf "sysctl configuration:\n"
printf "The following should be set to 0:\n\n"

sysctl --all | grep net.ipv4.conf.default.send_redirects
sysctl --all | grep net.ipv4.conf.all.send_redirects
sysctl --all | grep "net.ipv4.ip_forward "
sysctl --all | grep net.ipv4.conf.all.accept_source_route
sysctl --all | grep net.ipv4.conf.all.accept_redirects
sysctl --all | grep net.ipv4.conf.all.secure_redirects
sysctl --all | grep fs.suid_dumpable 

printf "\nThe following should be set to 1:\n\n" 
sysctl --all | grep net.ipv4.conf.all.log_martians
sysctl --all | grep net.ipv4.conf.default.log_martians
sysctl --all | grep net.ipv4.icmp_echo_ignore_broadcasts
sysctl --all | grep net.ipv4.icmp_ignore_bogus_error_responses

printf "\nThe following should be set to 0:\n\n"
sysctl --all | grep net.ipv4.conf.default.accept_source_route
sysctl --all | grep net.ipv4.conf.all.accept_redirects
sysctl --all | grep net.ipv4.conf.default.secure_redirects
#sysctl --all | grep net.ipv4.icmp_echo_ignore_broadcasts
#sysctl --all | grep net.ipv4.icmp_ignore_bogus_error_responses
sysctl --all | grep net.ipv4.tcp_syncookies
sysctl --all | grep net.ipv4.conf.all.rp_filter
sysctl --all | grep net.ipv4.conf.default.rp_filter
sysctl --all | grep net.ipv6.conf.all.disable_ipv6
sysctl --all | grep "net.ipv6.conf.all.accept_ra "
sysctl --all | grep "net.ipv6.conf.default.accept_ra "
sysctl --all | grep net.ipv6.conf.all.accept_redirects
sysctl --all | grep net.ipv6.conf.default.accept_redirects

printf "========================================\n"
printf "other\n\n"
 
#are these running: rxinetd telnet-server rsh-server ypserv tftp-server
#find -type f -name .rhosts -exec rm -f '{}' \;
#rm /etc/hosts.equiv


printf "These services should be enabled and active"
SERVICES=( chronyd rsyslog psacct sshd smartd )
for i in "${SERVICES[@]}"
do
    printf "%s:\n" ${i}
    systemctl is-enabled ${i}
    systemctl is-active  ${i}
    printf "\n"
done

printf "These services should be disabled and inactive unless needed"
SERVICES=( cups )
for i in "${SERVICES[@]}"
do
    printf "%s:\n" ${i}
    systemctl is-enabled ${i}
    systemctl is-active  ${i}
    printf "\n"
done

SERVICES=( xinetd rexec ypbind tftp abrtd ntpdate oddjobd qpidd rdisc sysstat atd bluetooth )
for i in "${SERVICES[@]}"
do
    printf "%s:\n" ${i}
    systemctl is-enabled ${i}
    systemctl is-active  ${i}
    printf "\n"
done

SERVICES=( autofs httpd vsftpd kdump.service cockpit.socket )
for i in "${SERVICES[@]}"
do
    printf "%s:\n" ${i}
    systemctl is-enabled ${i}
    systemctl is-active  ${i}
    printf "\n"
done

#systemctl status $i

#service $i status
#chkconfig --list | grep $i


EOF


#####################
## DEPLOY NEW FILES
#####################

/bin/cp -f ${BACKUPDIR}/CONFIG.conf /etc/CONFIG.conf
/bin/chown root:root /etc/CONFIG.conf
/bin/chmod       600 /etc/CONFIG.conf


#timestamp
echo "** fedora-oscap COMPLETE" $(date +%F-%H%M-%S)

%end
